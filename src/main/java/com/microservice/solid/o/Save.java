package com.microservice.solid.o;

public interface Save {
    void save(Order order);
}
