package com.microservice.solid.o;

public class SaveOrderToCloud implements Save{
    public void save(Order order){
        System.out.println("Server order to cloud id: " + order.getId() + " address: " + order.getAddress());
    }
}
