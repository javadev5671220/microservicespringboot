package com.microservice.solid.o;

public class SaveOrderToDb implements Save{
    public void save(Order order){
        System.out.println("Save order to database id: " + order.getId() + " address: " + order.getAddress());
    }
}
