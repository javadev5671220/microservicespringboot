package com.microservice.solid.o;

public class Order {
    private final Long id;
    private final String address;
    public Order(Long id, String address){
        this.id=id;
        this.address=address;
    }

    public Long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }
}
