package com.microservice.solid.d.bad;

public class MySQL {
    public void save(ModelForm modelForm){
        System.out.println("Save to db from: " + modelForm);
    }
    public void save(Loan loan){
        System.out.println("Save to db from: " + loan);
    }
    public void save(Payment payment){
        System.out.println("Save to db from: " + payment);
    }
    public void save(Order order){
        System.out.println("Save to db from: " + order);
    }
}
