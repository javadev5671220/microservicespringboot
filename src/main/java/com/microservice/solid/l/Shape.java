package com.microservice.solid.l;

public interface Shape {
    int getArea();
}
