package com.microservice.solid.l;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Shape rectangle = new Rectangle(12,34);
        Shape square = new Square(13);

        List<Shape> shapes = List.of(rectangle,square);
//        for (Shape it: shapes){
//            System.out.println(it.getArea());
//        }
        shapes.forEach(it -> {
            System.out.println(it.getArea());
        });
    }
}
